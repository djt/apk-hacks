import subprocess, os, time
from pprint import pprint

os.chdir('{}/aports'.format(os.environ['HOME']))
aports = os.getcwd()
repos = ['main', 'community']
pkg_vars = ['pkgver',
            'provides', 'subpackages',
            'makedepends', 'checkdepends']

def gen_cmd():
    init = 'source APKBUILD'
    commands = ['echo ${}'.format(var) for var in pkg_vars]
    return '&&'.join([init, *commands])

def get_pkg_vars(package):
    sh = subprocess.run(gen_cmd(), shell=True, stdout=subprocess.PIPE)
    # we always want to cut off the last newline
    # newlines separate individual variables
    output = sh.stdout.decode('utf-8')[:-1].split('\n')
    if len(output) != len(pkg_vars):
        raise Exception('Did not get expected output '
                         'for package "{}":\n{}\n---'.format(package, output))
    return dict(zip(pkg_vars, output))

print('aports directory is {}'.format(aports))

pkg_cache = {}

for repo in repos:
    pkgs = os.scandir("{}/{}".format(aports, repo))
    for pkg in pkgs:
        if pkg.is_dir() and pkg.name[0] != '.':
            os.chdir(pkg.path)
            pkg_cache[pkg.name] = get_pkg_vars(pkg.name)

pprint(pkg_cache)
